import requests, threading, time

src = input ("Starting Wikipedia URL: ")
end = input ("Target Wikipedia URL: ")

domain = src.split ("/wiki")[0]
maxthreads = 12
found = 0

seen = {}
q = []
q.append (["", src])

def crawl (entry):
	global seen
	global found

	if (found):
		return

	url = entry[1]
	path = entry[0]
	path += "\x00" + url
	clicks = len (path.split ("\x00")) - 2
	print (str (clicks) + ": " + url)

	if (url == end):
		print ("Found! - {} Clicks!".format (clicks))
		found = 1

		for link in path.split ("\x00"):
			print (link)

	r = requests.get (url).text
	r = r[r.find ("mw-parser-output") + len ("mw-parser-output"):]
	r = r[:r.find ("mw-references-wrap")]
	r = r.split ('href="/wiki')

	for suburl in r:
		suburl = suburl[:suburl.find ('"')].strip ()
		suburl = suburl.split ("#")[0]

		if (len (suburl) == 0):
			continue

		if (":" not in suburl):
			if suburl not in seen:
				seen[suburl] = 1
				suburl = domain + "/wiki" + suburl
				q.append ([path, suburl])

while True:
	try:
		data = q.pop (0)
		thr = threading.Thread (target = crawl, args = (data,))
		thr.daemon = True
		thr.start ()

	except Exception as e:
		pass

	while (threading.active_count () >= maxthreads):
		time.sleep (0.1)

	if (found):
		break

print ("Waiting for daemons to finish...")
while (threading.active_count () > 0):
	time.sleep (1)